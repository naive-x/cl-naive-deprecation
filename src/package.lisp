(in-package :common-lisp-user)

(defpackage :cl-naive-deprecation
  (:use :cl)
  (:export

   :deprecation-warning
   :deprecation-warning-operator-name
   :deprecation-warning-operator-kind
   :deprecation-warning-source-file 
   :deprecation-warning-source-form
   :removed-warning
   :replaced-warning
   :replaced-warning-replacement-name
   :replaced-warning-replacement-description

   :declare-deprecation
   :declare-deprecated-operators
   :deprecated-operators
   :report-deprecations
   :clear-deprecations
   ;; internal:
   :*deprecation-warnings*))
