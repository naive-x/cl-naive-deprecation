(in-package :cl-naive-deprecation)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *deprecation-warnings* t
    "If true, warnings are issued when deprecated operators are used.")
  (defvar *deprecation-locations* (make-hash-table :test 'equal)
    "A hash table mapping deprecated operator names to lists of locations where they were used.
Note: we may deprecate (setf foo) operators too."))


(define-condition simple-program-error (program-error simple-error)
  ())

(define-condition deprecation-warning (warning) 
  ((operator-name :initarg :operator-name
                  :reader deprecation-warning-operator-name)
   (operator-kind :initarg :operator-kind
                  :initform 'operator
                  :reader deprecation-warning-operator-kind)
   (source-file   :initarg :source-file
                  :initform nil
                  :reader deprecation-warning-source-file)
   (source-form   :initarg :source-form
                  :initform nil
                  :reader deprecation-warning-source-form))
  (:report (lambda (condition stream)
             (format stream "~:(~A~) ~S has been deprecated~@[, but is used in ~A~]~@[: ~S~]"
                     (deprecation-warning-operator-kind condition)
                     (deprecation-warning-operator-name condition)
                     (deprecation-warning-source-file condition)
                     (deprecation-warning-source-form condition)))))

(define-condition removed-warning (deprecation-warning)
  ()
  (:report (lambda (condition stream)
             (format stream "~:(~A~) ~S has been removed without replacement~@[, but is used in ~A~]~@[: ~S~]"
                     (deprecation-warning-operator-kind condition)
                     (deprecation-warning-operator-name condition)
                     (deprecation-warning-source-file condition)
                     (deprecation-warning-source-form condition)))))

(define-condition replaced-warning (deprecation-warning)
  ((replacement-name :initarg :replacement-name
                     :reader replaced-warning-replacement-name)
   (replacement-description :initarg :replacement-description
                            :reader replaced-warning-replacement-description))
  (:report (lambda (condition stream)
             (format stream "~:(~A~) ~S has been replaced by ~S~@[, but is used in ~A~]~@[: ~S~]~@[~%~A~]"
                     (deprecation-warning-operator-kind condition)
                     (deprecation-warning-operator-name condition)
                     (replaced-warning-replacement-name condition)
                     (deprecation-warning-source-file condition)
                     (deprecation-warning-source-form condition)
                     (replaced-warning-replacement-description condition)))))

;; Would it make sense to have a couple of different messages for different reasons like
;; Function [function name] has been depreciated.
;; Function [function name] was removed, there is no replacement.
;; Function [function name] was replaced with [other function name or description of how to convert to new funcitonality].
;; When one function replaces another and we know how to convert from the one to the other can we inject converted code instead?


#-(and)
(declare-deprecation
 (macro moo removed)
 (function foo deprecated)
 (generic-function goo replaced-by hoo)
 (function foo (x y z) replaced-by hoo as
           (let ((vx (gensm))
                 (va (gensym))
                 (vb (gensym)))
             `(let* ((,vx ,x)
                     (,va (+ ,y ,z))
                     (,vb (+ ,vx (* 2 ,va))))
                (hoo ,va ,vb)))))

#|

(declare-deprecation deprecation-clause &rest other-deprecation-clauses)

deprecation-clause ::= (operator-kind operator-name &rest deprecation-reason)
operator-kind ::= macro | function | generic-function
deprecation-reason ::= (deprecated | removed | replaced-by replacement-operator-name [ as [description-string] &body replacement-body ])

|#

(eval-when (:compile-toplevel :load-toplevel :execute)

  (defun syntax-keyword (symbol &optional expected)
    "Intern the name of the symbol in the source package.
If expected (a list of symbols) is given, then check that symbol has
the same name as one of the symbols in expected, and return the symbol
from expected."
    (let ((result (find symbol expected :test (function string-equal))))
      (unless (and (symbolp symbol)
                   (or (null expected) result))
        (error 'simple-program-error
               :format-control "Expected a symbol~@[ whose name is one of ~{~A~^, ~}~], not a ~S such as ~S"
               :format-arguments (list  expected (type-of symbol) symbol)))
      (if expected
          result
          (intern (symbol-name symbol)
                  (load-time-value (find-package :cl-naive-deprecation))))))

  (defun generate-deprecated-operator (kind name)
    ;; Function [function name] has been depreciated.
    `(define-compiler-macro ,name (&whole form &rest arguments)
       (declare (ignore arguments))
       (let ((where-from (or *compile-file-truename*
                             *load-truename*)))
         (when *deprecation-warnings*
           (warn 'deprecation-warning
                 :operator-kind ',kind
                 :operator-name ',name
                 :source-file where-from
                 :source-form form))
         (when where-from
           (pushnew where-from (gethash ',name *deprecation-locations*)
                    :test (function equalp)))
         form)))

  (defun generate-removed-operator (kind name)
    ;; Function [function name] was removed, there is no replacement.
    `(define-compiler-macro ,name (&whole form &rest arguments)
       (declare (ignore arguments))
       (let ((where-from (or *compile-file-truename*
                             *load-truename*)))
         (when *deprecation-warnings*
           (warn 'removed-warning
                 :operator-kind ',kind
                 :operator-name ',name
                 :source-file where-from
                 :source-form form))
         (when where-from
           (pushnew where-from (gethash ',name *deprecation-locations*)
                    :test (function equalp)))
         `(error "~:(~A~) ~S was removed without replacement" ,',kind ,',name))))

  (defun generate-replaced-by-operator (kind name lambda-list-p lambda-list replacement-clause)
    ;; Function [function name] was replaced with [other function name or description of how to convert to new funcitonality].
    ;; When one function replaces another and we know how to convert from the one to the other can we inject converted code instead?
    (let ((replacement-name   (pop replacement-clause))
          (description-string (when replacement-clause
                                (syntax-keyword (pop replacement-clause) '(as))
                                (when (and replacement-clause
                                           (stringp (first replacement-clause)))
                                  (pop replacement-clause))))
          (body               replacement-clause)
          (vform       (gensym "FORM"))
          (varguments  (gensym "ARGUMENTS"))
          (vwhere-from (gensym "WHERE-FROM")))
      (when (and lambda-list-p
                 (not (null lambda-list))
                 (null body))
        (error 'simple-program-error
               :format-control "Replacement for ~A has lambda-list but no body"
               :format-arguments (list name)))
      `(define-compiler-macro ,name (&whole ,vform ,@(if lambda-list-p lambda-list `(&rest ,varguments)))
         ,@(unless lambda-list-p `((declare (ignorable ,varguments))))
         (let ((,vwhere-from (or *compile-file-truename*
                                 *load-truename*)))
           (when *deprecation-warnings*
             (warn 'replaced-warning
                   :operator-kind ',kind
                   :operator-name ',name
                   :replacement-name ',replacement-name
                   :replacement-description ',description-string
                   :source-file ,vwhere-from
                   :source-form ,vform))
           (when ,vwhere-from
             (pushnew ,vwhere-from (gethash ',name *deprecation-locations*)
                      :test (function equalp)))
           ,(if body
                `(progn ,@body)
                (list 'cons (list 'quote replacement-name) varguments))))))

  ) ;;eval-when


(defmacro declare-deprecation (deprecation-clause &rest other-deprecation-clauses)
  `(progn
     ,@(loop :for (operator-kind operator-name . deprecation-reason)
             :in (cons deprecation-clause other-deprecation-clauses)
             :do (setf (gethash operator-name *deprecation-locations*)
                       (gethash operator-name *deprecation-locations* '()))
             :collect (let* ((lambda-list-p  (listp (first deprecation-reason)))
                             (lambda-list    (when lambda-list-p
                                               (pop deprecation-reason))))
                        (flet ((check-no-lambda ()
                                 (when lambda-list-p
                                   (error 'simple-program-error
                                          :format-control "Expected a symbol~@[ whose name is one of ~{~A~^, ~}~], not a ~S such as ~S"
                                          :format-arguments (list '(deprecated removed replaced-by)
                                                                  (type-of lambda-list) lambda-list)))))
                          (case (syntax-keyword (first deprecation-reason)
                                                '(deprecated removed replaced-by))
                            ((deprecated)
                             (check-no-lambda)
                             (generate-deprecated-operator  operator-kind operator-name))
                            ((removed)
                             (check-no-lambda)
                             (generate-removed-operator     operator-kind operator-name))
                            ((replaced-by)
                             (generate-replaced-by-operator operator-kind operator-name
                                                            lambda-list-p lambda-list
                                                            (rest deprecation-reason)))))))))


(defmacro declare-deprecated-operators (&rest operator-names)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     ,@(loop :for name :in operator-names
             ;; we already record at compilation time
             ;; that the operator is declared as deprecated:
             :do (setf (gethash name *deprecation-locations*)
                       (gethash name *deprecation-locations* '()))
                 ;; Then we generate the compiler macro, in case we effectively use it.
             :collect `(define-compiler-macro ,name (&whole form &rest arguments)
                         (declare (ignore arguments))
                         (let ((where-from (or *compile-file-truename*
                                               *load-truename*)))
                           (when *deprecation-warnings*
                             (warn 'deprecation-warning
                                   :operator-name ',name
                                   :source-file where-from
                                   :source-form form))
                           (when where-from
                             (pushnew where-from (gethash ',name *deprecation-locations*)
                                      :test (function equalp)))
                           form)))
     (values)))

(defun deprecated-operators ()
  "Return a list of the names of all the deprecated operators."
  (let ((operators '()))
    (maphash (lambda (operator locations)
               (declare (ignore locations))
               (pushnew operator operators))
             *deprecation-locations*)
    operators))

(defun report-deprecations (&key (stream *standard-output*) (include-unused nil))
  "Prints on the stream a list of all the deprecated operators
that were used and the files they're used in."
  (maphash (lambda (operator locations)
             (if (or locations include-unused)
                 (if locations
                     (format stream "~&~S is deprecated but used in:~{~%    ~A~}~%" operator locations)
                     (format stream "~&~S is deprecated~%" operator))))
           *deprecation-locations*)
  (values))

(defun clear-deprecations ()
  "Clear the list of deprecated operators and the locations where they were used."
  (clrhash *deprecation-locations*))


