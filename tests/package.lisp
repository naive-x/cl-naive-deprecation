(in-package :common-lisp-user)

(defpackage :cl-naive-deprecation.tests
  (:use :cl :cl-naive-tests :cl-naive-deprecation)
  (:export))

(defpackage :cl-naive-deprecation.tests.subject
  (:use :cl :cl-naive-deprecation)
  (:export :foo :moo :goo))
