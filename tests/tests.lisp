(in-package :cl-naive-deprecation.tests)

(defun set-equal (a b) (and (subsetp a b) (subsetp b a)))

(testsuite  :utility

  (testcase :syntax-keyword
            :expected '(foo cl-naive-deprecation::foo program-error)
            :actual (list (cl-naive-deprecation::syntax-keyword :foo '(foo bar baz))
                          (cl-naive-deprecation::syntax-keyword :foo)
                          (handler-case (cl-naive-deprecation::syntax-keyword :quux '(foo bar baz))
                            (:no-error (&rest results) (declare (ignore results)) 'no-error)
                            (program-error (err) 'program-error)
                            (t () 'condition)))))

(testsuite  :deprecation

  
  (testcase :compiling-and-deprecated-operators
            :equal (lambda (a b)
                     (and (every (function set-equal) (butlast a) (butlast b))
                          (equalp (last a) (last b))))
            :expected '((cl-naive-deprecation.tests.subject:goo
                         cl-naive-deprecation.tests.subject:moo
                         cl-naive-deprecation.tests.subject:foo)
                        (cl-naive-deprecation.tests.subject:goo
                         cl-naive-deprecation.tests.subject:moo
                         cl-naive-deprecation.tests.subject:foo)
                        (cl-naive-deprecation.tests.subject:goo
                         cl-naive-deprecation.tests.subject:moo
                         cl-naive-deprecation.tests.subject:foo)
                        (cl-naive-deprecation.tests.subject:goo
                         cl-naive-deprecation.tests.subject:moo
                         cl-naive-deprecation.tests.subject:foo)
                        (("cl-naive-deprecation/tests/user.lisp" (cl-naive-deprecation.tests.subject:goo 42))
                         ("cl-naive-deprecation/tests/user.lisp" (cl-naive-deprecation.tests.subject:foo (quote 42)))
                         ("cl-naive-deprecation/tests/user.lisp" (cl-naive-deprecation.tests.subject:moo 42))
                         ("cl-naive-deprecation/tests/user.lisp" (cl-naive-deprecation.tests.subject:foo 42))
                         ("cl-naive-deprecation/tests/operators.lisp" (cl-naive-deprecation.tests.subject:foo cl-naive-deprecation.tests.subject::x))
                         ("cl-naive-deprecation/tests/operators.lisp" (cl-naive-deprecation.tests.subject:foo (quote cl-naive-deprecation.tests.subject::x)))
                         ("cl-naive-deprecation/tests/operators.lisp" (cl-naive-deprecation.tests.subject:moo cl-naive-deprecation.tests.subject::x))))
            :actual (let ((warnings '()))
                      (handler-bind ((deprecation-warning
                                       (lambda (c)
                                         (push (list
                                                (namestring
                                                 (let ((path (deprecation-warning-source-file c)))
                                                   (make-pathname :directory (list* :relative (last (pathname-directory path) 2))
                                                                  :defaults path)))
                                                (deprecation-warning-source-form c))
                                               warnings)
                                         (invoke-restart 'muffle-warning))))
                        (clear-deprecations)
                        (remove t
                                (append
                                 (let ((fasl (compile-file
                                              (merge-pathnames "tests/operators.lisp"
                                                               (asdf:system-source-directory "cl-naive-deprecation.tests")))))
                                   (list (deprecated-operators)
                                         (load fasl)
                                         (deprecated-operators)))
                                 (let ((fasl (compile-file
                                              (merge-pathnames "tests/user.lisp"
                                                               (asdf:system-source-directory "cl-naive-deprecation.tests")))))
                                   (list (deprecated-operators)
                                         (load fasl)
                                         (deprecated-operators)))
                                 (list warnings)))))))

  


#-(and)
(progn (ql:quickload :cl-naive-deprecation.tests)
       (cl-naive-tests:run)
       (cl-naive-tests:write-results cl-naive-tests:*suites-results* :format :text))
