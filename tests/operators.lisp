(in-package :cl-naive-deprecation.tests.subject)

(declare-deprecated-operators foo moo goo)

(defun foo (x)
  (list x))

(defmacro moo (x)
  `(foo ',x))
  
(defgeneric goo (x))
(defmethod goo ((x t))
  (list (moo x) (foo x)))

