(defsystem "cl-naive-deprecation.tests"
  :description ""
  :version "2023.8.23"
  :author "Phil Marneweck"
  :licence "MIT"
  :depends-on ("cl-naive-tests" "cl-naive-deprecation")
  :components ((:file "tests/package")
               (:file "tests/operators" :depends-on ("tests/package"))
               (:file "tests/user"      :depends-on ("tests/package"))
               (:file "tests/tests"     :depends-on ("tests/package"
                                                     "tests/operators"
                                                     "tests/user"))))

