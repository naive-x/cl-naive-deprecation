(in-package :cl-user)
(ql:quickload :cl-naive-deprecation)
(use-package :cl-naive-deprecation)

(declare-deprecated-operators foo moo goo)
(handler-bind ((deprecation-warning
                 (lambda (dep) 
                   (format t "~A~%" dep)
                   (invoke-restart  'muffle-warning))))
  (load (compile-file (merge-pathnames "example.lisp" *load-truename*))))

(goo 42)

(format t "Deprecated operators:")
(pprint (deprecated-operators))
(terpri)

(report-deprecations)
(finish-output)

(clear-deprecations)
(assert (null (deprecated-operators)))
