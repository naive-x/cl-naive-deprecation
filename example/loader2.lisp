(in-package :cl-user)
(ql:quickload :cl-naive-deprecation)
(use-package :cl-naive-deprecation)

(setf *print-right-margin* 80)

(load (compile-file (merge-pathnames "deprecations.lisp" *load-truename*)))

(handler-bind ((deprecation-warning
                 (lambda (dep) 
                   (format t "~A~%" dep)
                   (invoke-restart  'muffle-warning))))
  (load (compile-file (merge-pathnames "example.lisp" *load-truename*))))

(format t "Deprecated operators:")
(pprint (deprecated-operators))
(terpri)

(report-deprecations)
(finish-output)

(clear-deprecations)
(assert (null (deprecated-operators)))
