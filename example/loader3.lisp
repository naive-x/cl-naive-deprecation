(in-package :cl-user)
(ql:quickload :cl-naive-deprecation)
(use-package :cl-naive-deprecation)

(setf *print-right-margin* 80)

(clear-deprecations)

(load (compile-file (merge-pathnames "deprecations.lisp" *load-truename*)))
(load (compile-file (merge-pathnames "example.lisp" *load-truename*)))

(format t "~%Deprecation Report~
           ~%------------------~2%")
(report-deprecations)
(finish-output)

(clear-deprecations)
