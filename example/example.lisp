(in-package :cl-user)


(defun foo (x)
  (list 'foo x))

(defmacro moo (x)
  `(foo ',x))

(defgeneric goo (x))

(defmethod goo ((x t))
  (moo x)
  (foo x))


(defun bar (x)
  (list 'bar x))

(defmacro mar (x)
  `(bar ',x))

(defgeneric gar (x))

(defmethod gar ((x t))
  (mar x)
  (bar x))


(defun quux (x)
  (list 'quux x))

(defmacro muux (x)
  `(quux ',x))

(defgeneric guux (x))

(defmethod guux ((x t))
  (muux x)
  (quux x))



(defun new-quux (x y)
  (list 'quux x y))

(defmacro new-muux (x y)
  `(quux (list ',x ',y)))

(defgeneric new-guux (x y))

(defmethod new-guux ((x t) y)
  (new-muux x y)
  (new-quux x y))



