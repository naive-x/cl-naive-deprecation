(in-package :cl-user)

(declare-deprecation
 (function         bibi removed)
 (macro            mimi removed)
 (generic-function gigi removed)

 (function         bar deprecated)
 (macro            mar deprecated)
 (generic-function gar deprecated)
 
 (function         quux replaced-by foo)
 (macro            muux replaced-by moo)
 (generic-function guux replaced-by goo)
 )

(declare-deprecation
   
   (function         quux-2 (x) replaced-by new-quux as
                     "quux-2 is now new-quux with the double"
                     (let ((vx (gensym)))
                       `(let ((,vx ,x))
                          (new-quux ,vx (* 2 ,vx)))))
 
   (macro            muux-2 (x) replaced-by new-muux as
                     `(new-muux ,x xx))
 
   (generic-function guux-2 (x) replaced-by new-guux as
                     "guux-2 is now new-guux with the double"
                     (let ((vx (gensym)))
                       `(let ((,vx ,x))
                          (new-guux ,vx (* 2 ,vx)))))
   )
