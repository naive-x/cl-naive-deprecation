(defsystem "cl-naive-deprecation"
  :description ""
  :version "2023.8.23"
  :author "Phil Marneweck"
  :licence "MIT"
  :depends-on ()
  :components ((:file "src/package")
               (:file "src/deprecation" :depends-on ("src/package"))))

