* cl-naive-deprecation

We needed a generic way to mark depreciated functions in cl-naive-*
libraries and the following stack overflow post gave us some
inspiration.

https://stackoverflow.com/questions/71392756/best-practices-for-deprecating-functions

Of course the project took on a life of its own and we have the
following.

The =cl-naive-deprecation= package exports a facility to mark some
operators (function, generic function or macro), as being deprecated.

A compiler-macro is added to those operators, so that when the
operator is used, a warning is produced at compilation time and the
source file where it's used is recorded.

You can also supply code to replace the replaced operator with the new
code to use.

We can get the list of deprecated operators and we may print a report.

You can mark operators as deprecated, removed or replaced. 

** Alternatives Available to cl-naive-deprecation

https://shinmera.github.io/trivial-deprecate/ (only found this when
submitting this project to quicklisp, pure fluke!).

** Design

N/A

** Use Cases

Erm, deprecation?

** Documentation

[[docs/docs.org][Documentation]] can be found in the docs folder in the repository.


** Tutorials

Simple step by step [[file:docs/tutorials.org][tutorial(s)]] to get you started fast.

** How To(s)

How to do different things can be found [[file:docs/how-tos.org][here]].


** Dependencies

None.

** Supported CL Implementations

Should support all compliant implementations, no implementation specific code was used.

** Tests

To load and run the tests, clone the project and then:

#+BEGIN_SRC lisp
  (ql:quickload :cl-naive-deprecation.tests)

  (cl-naive-tests:report (cl-naive-tests:run))
#+END_SRC
