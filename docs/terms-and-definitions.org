# -*- mode:org;coding:utf-8 -*-

#+AUTHOR: Phil Marneweck
#+EMAIL: [Email]
#+DATE: Wed Aug 23 11:00:00 CEST 2023
#+TITLE: User Manual for the cl-naive-deprecation library

#+BEGIN_EXPORT latex
\clearpage
#+END_EXPORT

#+LATEX_HEADER: \usepackage[english]{babel}
#+LATEX_HEADER: \usepackage[autolanguage]{numprint} % Must be loaded *after* babel.
#+LATEX_HEADER: \usepackage{rotating}
#+LATEX_HEADER: \usepackage{float}
#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \usepackage[margin=0.75in]{geometry}

# LATEX_HEADER: \usepackage{indentfirst}
# LATEX_HEADER: \setlength{\parindent}{0pt}
#+LATEX_HEADER: \usepackage{parskip}

#+LATEX_HEADER: \usepackage{tikz}
#+LATEX_HEADER: \usetikzlibrary{positioning, fit, calc, shapes, arrows}
#+LATEX_HEADER: \usepackage[underline=false]{pgf-umlsd}
#+LATEX_HEADER: \usepackage{lastpage}
#+LATEX_HEADER: \pagestyle{fancyplain}
#+LATEX_HEADER: \pagenumbering{arabic}
#+LATEX_HEADER: \lhead{\small{cl-naive-deprecation}}
#+LATEX_HEADER: \chead{}
#+LATEX_HEADER: \rhead{\small{[Document Name]}}
#+LATEX_HEADER: \lfoot{}
#+LATEX_HEADER: \cfoot{\tiny{\copyright{2023 - 2023 Phil Marneweck}}}
#+LATEX_HEADER: \rfoot{\small{Page \thepage \hspace{1pt} de \pageref{LastPage}}}

* Terms and Definitions

This section is about the terms we use in our documentation.

** Deprecation

[[https://en.wikipedia.org/wiki/Deprecation][Wikipedia: Deprecation]]

/*In several fields, especially computing, deprecation is the discouragement of use of some terminology, feature, design, or practice, typically because it has been superseded or is no longer considered efficient or safe, without completely removing it or prohibiting its use. Typically, deprecated materials are not completely removed to ensure legacy compatibility or back up practice in case new methods are not functional in an odd scenario.

It can also imply that a feature, design, or practice will be removed or discontinued entirely in the future.*/

*** What do we agree with 

We need to warn users some how.

*** Where do we differ 

N/A

*** Conclusion

If only users would head deprecation warnings (in time).

# Local Variables:
# eval: (auto-fill-mode 1)
# End:
