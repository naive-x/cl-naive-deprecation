all: test documentation
.PHONY:: test tests documentation

TEST_SYSTEM = :cl-naive-deprecation.tests

# default values:
ARTDIR = tests/artifacts/
DEPENDENCYDIR = $(abspath $(CURDIR)/..)/
THISDIR = $(abspath $(CURDIR))/

test tests:
	sbcl --noinform --no-userinit --non-interactive \
		--eval '(proclaim (quote (optimize (safety 3) (debug 3) (space 0) (speed 0) (compilation-speed 3))))' \
		--eval '(load #P"~/quicklisp/setup.lisp")' \
		--eval '(push "$(DEPENDENCYDIR)" ql:*local-project-directories*)' \
		--eval '(push #P"$(THISDIR)" asdf:*central-registry*)' \
		--eval '(ql:quickload $(TEST_SYSTEM))' \
		--eval '(setf cl-naive-tests:*verbose* :trace)' \
		--eval '(write-line "before")' --eval '(force-output)' \
		--eval '(cl-naive-tests:run :debug nil)' \
		--eval '(write-line "mid")' --eval '(force-output)' \
		--eval '(cl-naive-tests:run :debug nil)' \
		--eval '(write-line "after")' --eval '(force-output)' \
		--eval '(cl-naive-tests:write-results cl-naive-tests:*suites-results* :format :text)' \
		--eval '(cl-naive-tests:save-results cl-naive-tests:*suites-results* :file "$(ARTDIR)junit-results.xml" :format :junit)' \
		--eval '(sb-ext:exit :code (if (cl-naive-tests:report) 0 200))'
documentation:
	make -C docs pdfs

clean:
	-rm *~ system-index.txt
	-find . \( -name *.[dlw]x[36][24]fsl -o -name --version.lock \) -exec rm {} +
	make -C docs clean

help:
	@printf 'make tests [DEPENDENCYDIR=…]  # run the tests.\n'
	@printf 'make documentation            # builds the pdf documentation.\n'
	@printf 'make help                     # builds this help.\n'
	@printf 'make all   [DEPENDENCYDIR=…]  # runs the tests and builds the pdf.\n'
